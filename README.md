

# Azure Enable Diagnostics

azure-enable-diagnostics automates enabling Monitoring Diagnostics on Azure VMs.

#### For each subscription assigned to the entered Azure login credentials:
* Register the Resource Provider: "Microsoft.Insights"
    1. In the navigation pane on the left, click Subscriptions
    2. Select the subscription identified in the error message
    3. Click Resource Providers
    4. Find the Microsoft.insights provider
    5. Click the Register link* 
* Choose a Standard storage account or script will create one to use for each resource group location to store the metrics from the extension

#### The user can control how the diagnostic agents are enabled:
* which subscriptions
* which storage account
* diagnostics storage account grouping methodology:
> The storage account is like an administrative container, and within that, several services like blobs, files, queues, tables, disks, etc can be included. When a storage account is created in Azure, all the storage resources inside it will have a unique namespace. It can either store data within a particular region or need separate billing for different data categories. Integrating data services into a storage account allows the user to manage them as a group. Once the storage account gets deleted, all the data stored inside gets removed.
    * per resource group per location
> created per location - where all the resources in that region use this storage account as sink or it can be created per resource group in a location. This can be used when there is no need to categorize each resource group's bill
    * per location
> created per resource group in a location - Where the data from the diagnostic agent from all the resources in that group will be pushed to that storage account. This enables to categorize the storage and bill from the storage account into a seperate namespace
* diagnostics storage account name with a prefix
* which VMs: Classic/Arm (Deployment Model) 
* which OS: Windows/Linux
* whether to override existing diagnostics settings

The outcome of the script is logged to a new timestamped file in the log/ folder of the script directory.
If the VM is not running then the agent will not be installed, VM will be skipped with a message that VM is in a shutdown state


## Prerequisites
The following installation tasks are required once-off.
Start "Windows PowerShell" with "Run as Administrator"
```PowerShell
Install-Module Azure
Install-Module AzureRM -Force
```
When trying to install the above modules, powershell will prompt
```Powershell
PowerShellGet requires NuGet provider
[Y] Yes  [N] No  [S] Suspend  [?] Help (default is "Y"):
```
Please press Y to continue the installation

## Parameters
* **DeploymentModel**
    * "Arm" / "Classic"
    * Mandatory
    * Arm refers to "Azure Resource Manager"
    * Classic refers to "Service Manager"
* **OsType**
    * "Windows" / "Linux"
    * Default = both
* **ChooseSubscription**
    * Prompt the user to choose which subscriptions assigned to the current user should have their subscriptions enabled, if not all the subscriptions will be enabled.
    * Default = true
* **ChooseStorage**
    * Prompt the user to choose/create which Standard Storage Accounts should be used per ResourceGroup-Location combination.
    * Default = false
* **StoragePerLocation**
    * Use a single Diagnostics Storage Account per location
    * Default = Use a Diagnostics Storage Account per ResourceGroup per location
* **StoragePrefix**
    * Optional String
    * Create and use diagnostics storage account named with prefix passed. Uses SA already existing in the region with prefix if StoragePerLocation is true else will pick from the resource group
    * Creates a ResouceGroup with StoragePrefix as its name when StoragePerLocation is true and a single Diagnostics Storage Account in that ResourceGroup is used for all VMs in the location
    * Default = ''
* **ChooseVM**
    * Prompt the user to choose which VMs should have Diagnostics enabled.
    * Default = false
* **OverrideDiagnostics**
    * Whether to override diagnostics of VMs that already have diagnostic settings enabled.
    * Default = false


### Examples

#### Preferred Default Command
This command enables the Diagnostics on all ARM specific VMs, within all subscriptions.
Will create a new storage account with prefix as 'lk' and uses that storage account to store all the metrics in that Subscription - Resource Group - Location
```PowerShell
\EnableDiag.ps1 –DeploymentModel 'Arm' -StoragePerLocation -StoragePrefix 'lk'
```


Enable Diagnostics on all ARM specific VMs, within specific subscriptions.
Automatically determine storage account to be used. Use a single storage account per location
```PowerShell
.\EnableDiag.ps1 –DeploymentModel 'Arm' -StoragePerLocation
```
Enable Diagnostics on all ARM specific VMs, within specific subscriptions.
Automatically determine storage account to be used. Use a single storage account per location
```PowerShell
.\EnableDiag.ps1 –DeploymentModel 'Arm' -ChooseSubscription -StoragePerLocation
```
Enable Diagnostics on all ARM specific VMs, within all subscriptions.
Automatically creates storage account and determines the one to use if already exists with the mentioned prefix. Use a single storage account per resource group + location
```PowerShell
.\EnableDiag.ps1 –DeploymentModel 'Arm' -StoragePrefix 'prefix'
```

Enable Diagnostics on all ARM specific VMs, within all subscriptions.
Automatically creates storage account and determines the one to use if already exists with the mentioned prefix. Use a single storage account per location.
```PowerShell
.\EnableDiag.ps1 –DeploymentModel 'Arm' -StoragePerLocation -StoragePrefix 'prefix'
```
Enable Diagnostics on specific ARM VMs, within specific subscriptions.
Automatically determine storage account to be used. Use storage account per resource group + location
```PowerShell
.\EnableDiag.ps1 –DeploymentModel 'Arm' -ChooseSubscription -ChooseVM
```
Enable Diagnostics on specific Classic VMs, within specific subscriptions, choosing storage account resource group + location
```PowerShell
.\EnableDiag.ps1 –DeploymentModel 'Classic' -ChooseSubscription -ChooseVM -ChooseStorage
```
Enable Diagnostics on specific Classic Linux VMs, within specific subscriptions, overriding existing diagnostic settings. Use storage account per location per resource group.
Automatically determine storage account to be used
```PowerShell
.\EnableDiag.ps1 –DeploymentModel 'Classic' -OsType 'Linux' -OverrideDiagnostics
```
---
**Few minutes after installation in a VM**
* The successful installation of the extension can be verified by logging into the Azure portal
* In Virtual Machines, view your list of your VMs and then select a VM.
* Under Monitoring, select Metrics.
* In the metrics chart under Metric Namespace, select Guest (Classic).
* In the Metric list, you can view all of the available performance counters for the guest VM.
* Metrics mentioned in the configuration file must appear in the dropdown
* On selecting a metric, if the VM is running then the graph should show up.

# Troubleshooting

## Solution for unable to install NuGet provider for PowerShell
When trying to install the Azure module, the following error message - "Unable to find package provider ‘NuGet’" may appear.
We will need to enable TLS 1.2 on the system. Run both cmdlets to set .NET Framework strong cryptography registry keys. After that, restart PowerShell and check if the security protocol TLS 1.2 is added. As of last, install the PowerShellGet module.

The first cmdlet is to set strong cryptography on 64 bit .Net Framework (version 4 and above).
```PowerShell
PS C:> Set-ItemProperty -Path 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\.NetFramework\v4.0.30319' -Name 'SchUseStrongCrypto' -Value '1' -Type DWord
```
The second cmdlet is to set strong cryptography on 32 bit .Net Framework (version 4 and above).
```PowerShell
PS C:> Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\.NetFramework\v4.0.30319' -Name 'SchUseStrongCrypto' -Value '1' -Type DWord
```
Restart Powershell and check for supported security protocols.
```PowerShell
PS C:\>[Net.ServicePointManager]::SecurityProtocol
Tls, Tls11, Tls12
```
## Login script error
 ‘An error has occurred in the script of this page’ may appear while the login window for Azure is prompted

Please follow the instruction at https://support.microsoft.com/en-in/help/3135465/how-to-enable-javascript-in-windows

## Portal doesn’t support configuring VM diagnostics using JSON
After enabling diagnostics on a Classic VM, you may see the following message in the Portal UI:
"The Azure Portal currently doesn’t support configuring virtual machine diagnostics using JSON. Instead, use PowerShell or CLI to configure diagnostics for this machine".
This message is should automatically disappear when you recheck the VM a bit later.

## Possible issues while installing the agent
* Agent will not work in Unsupported distributions, only the following distributions are supported:
    * [Linux supported distros]( https://docs.microsoft.com/en-us/azure/virtual-machines/extensions/diagnostics-linux?toc=/azure/azure-monitor/toc.json#supported-linux-distributions)
    * [Windows supported versions](https://docs.microsoft.com/en-in/troubleshoot/azure/virtual-machines/extension-supported-os)
* If the script is taking longer than 10 mins to install the agent or to get the response then it because the VM has no internet access or if the VM has an issue accessing the apt-get or yum. In such cases, use -ChooseVM switch to skip that particular VM

# Customizing
The configuration can be overridden by custom-defined user diagnostics by replacing the winDiagConfig.xml file for Windows and linuxDiagConfig.json for Linux before running the script.

## Referenced articles
https://docs.microsoft.com/en-us/azure/virtual-machines/virtual-machines-windows-ps-extensions-diagnostics
https://docs.microsoft.com/en-us/azure/virtual-machines/virtual-machines-linux-classic-diagnostic-extension
