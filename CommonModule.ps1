﻿# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 

# MODIFICATIONS TO THIS FILE EFFECTED BY LEANKLOUD SOLUTIONS PRIVATE LIMITED DESCRIBED IN COMMENTS BELOW
# add a prefix for StorageAccountName provded as a parameter
# linux conifguraion file must be a json. changes were made to accomodate this - to read from confg file and return the configuration
# StorageName as parameter to GetDiagnosticsConfigPath because storage name is required in ConfigFile

New-Module -name CommonModule -ScriptBlock {
    function EnableLogging() {
    	Param (
            [Parameter(Mandatory=$true)]
            [string]$path
        )
    
        $logPath = $path + "/logs/" + ((Get-Date).ToUniversalTime()).ToString("yyyyMMddTHHmmssfffffffZ") + "_log.txt"
    	$ErrorActionPreference="SilentlyContinue"
    	Stop-Transcript | out-null
    	Start-Transcript -path $logPath -append
    }
 
    function ToSkip() {
    	Param (
    		[string]$Message,
            [bool]$DebugMode
    	)
    
    	if ($DebugMode){
    		Write-Host ($Message)

            $response = "y"
            
            while ($true) {
    		    $response = Read-Host ("Enter 'y' for yes (default), 'n' for no, 'a' to abort script")
                if (!$response) {$response = "y"}
                switch -Regex ($response) {
                    "(?i)^y(es)?$" {return $false}
                    "(?i)^n(o)?$" {return $true}
                    "(?i)^a(bort)?$" {exit}
                }
            }
    	}
    }
    
    function GetStorageName() {
    	Param (
    		[Parameter(Mandatory=$true)]
    		[string] $ResourceGroupName,
    		[Parameter(Mandatory=$true)]
    		[string] $Location,
            [bool]$ChooseStorage,
            [string]$StoragePrefix
    	)
    
    	$storageName = $null
    
    	if($ChooseStorage) {
    		Write-Host("Enter name for storage account for resource group '$ResourceGroupName' in location '$Location'")
            if (ToSkip "Use auto name generation?" $ChooseStorage) {
        		Write-Host("Name should contain only lowercase alphanumeric characters, at least 6 characters and maximum 24")
    		    $storageName = Read-Host ("Enter storage account name")

    			return $storageName
            }
    	} 
    
    	$storageName = $null
    
    	$charsOnly = $ResourceGroupName -replace '[^a-zA-Z0-9]', ''
        # if the resource group name is already StoragePrefix replace it with empty string
        # create storage account name with concatnating prefix, resource group name and random interger
        # random integer as StorageAccountName as StorageAccountName must be Unique in Azure
        # https://docs.microsoft.com/en-us/azure/storage/common/storage-account-overview#naming-storage-accounts
        $resName = $charsOnly -replace $StoragePrefix, ''
        $namePrefix = $StoragePrefix + $resName.ToLower()
        # storage account name cannot be more than 24 characters
        $substring = $namePrefix.Substring(0, (($namePrefix.Length, 14) | measure -Minimum).Minimum)
    	$storageName = $substring.ToLower() + "diag" + (Get-Random -Maximum 999999).ToString("000000");
    
    	return $storageName
     }
    
    function GetDiagnosticsConfigPath() {
	    Param (
	    	[Parameter(Mandatory=$true)]
	    	[string]$Path,
	    	[Parameter(Mandatory=$true)]
	    	[string]$VmId,
	    	[Parameter(Mandatory=$true)]
            [string]$OsType,
            [Parameter(Mandatory=$true)]
            [string]$storageName
	    )

        $xmlConfigPath = $null
        $jsonConfigPath = $null

        switch -regex ($OsType) {
            "[Ww]indows" {
                $xmlConfigPath = $Path + "/winDiagConfig.xml"
                $xmlConfig = [xml](Get-Content $xmlConfigPath)
                $xmlConfig.WadCfg.DiagnosticMonitorConfiguration.Metrics.SetAttribute("resourceId", $VmId)
                $tmpPath = [System.IO.Path]::GetTempFileName()
                $xmlConfig.Save($tmpPath)
            }
            "[Ll]inux" {
                $jsonConfigPath = $Path + "/linuxDiagConfig.json"
                $jsonConfig = Get-Content -Raw -Path $jsonConfigPath | ConvertFrom-Json
                $jsonConfig.StorageAccount = $storageName
                $jsonConfig.ladCfg.diagnosticMonitorConfiguration.metrics.resourceId = $VmId
                $tmpPath = [System.IO.Path]::GetTempFileName()
                $jsonConfig | ConvertTo-Json -depth 10 | Set-Content $tmpPath
            }
            default {throw [System.InvalidOperationException] "$OsType is not supported. Allowed values are 'Windows' or 'Linux' "}
        }
        return $tmpPath
    }

    Export-ModuleMember -Function EnableLogging
    Export-ModuleMember -Function ToSkip
    Export-ModuleMember -Function GetStorageName
    Export-ModuleMember -Function GetDiagnosticsConfigPath

} | Import-Module
# SIG # Begin signature block
# MIIFoQYJKoZIhvcNAQcCoIIFkjCCBY4CAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUExL+OpJWOor+61VsD8V4rtLo
# /tegggM2MIIDMjCCAhqgAwIBAgIQQ23G+earVbBFJncRAQu+UDANBgkqhkiG9w0B
# AQsFADAgMR4wHAYDVQQDDBV2YXNhbnRoQGxlYW5rbG91ZC5jb20wHhcNMjAxMTEy
# MTc1MTE0WhcNMjExMTEyMTgxMTE0WjAgMR4wHAYDVQQDDBV2YXNhbnRoQGxlYW5r
# bG91ZC5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDZv9r1hfIR
# +w1mFzarNbNo/ECgRlSsNKYnBcO7VEQBaS7qNmLkmjyIpwiopm+IzWJRTt8vRYQA
# PTUzXhv+63rwEYPEgCANdrbkb258R8N5RpV7YcOI4mK5ePjJi5Jdr80824pkmi9b
# ZV6E1FYAFIMoy88CEpjWKKdTp4xXbL1yeiBARRc+xmOtfaqRxVNNCFb75MV4ymwi
# gKNoxxAZoN0br6aNUBkyF5pc5l/30RlvWLKzDXwoWf8FMx/dBfhY2jkLZHU4Sh9W
# xzYHYgq82rTlL60AomgiuTkNQvxMa/We5jVv78NzC/ZfAzBeXKIHSztHHlykavgz
# ziJKD+YObrPdAgMBAAGjaDBmMA4GA1UdDwEB/wQEAwIHgDATBgNVHSUEDDAKBggr
# BgEFBQcDAzAgBgNVHREEGTAXghV2YXNhbnRoQGxlYW5rbG91ZC5jb20wHQYDVR0O
# BBYEFOislcM6IsEcO5y4eUCfhLezZrWbMA0GCSqGSIb3DQEBCwUAA4IBAQDXi9+m
# wWZFS+VADMFRpNnsE9LeAf9DgsB5XxeYlwow5KKdloNR46RD+y9GpHlBIDIWuwiG
# HjW1M1ISuz01P3/+xm6FakHl1gqK6B0MM2a2iysyg8LtzFJr11qYyNSuYa5GriWO
# rW6sMdTGwGTMoZ0HtRulPBzpwgjGGJr8ZD9ChcEvbbv/4zgTSXtjgIMmSGq0Qh8R
# b/YOalPY8JEqTq2SRfqNxWl59ommGejaZYCxVXK5QzWmHBdlc+52LuvrVr65Ikvw
# M56uj98G9QE3Hgy1c/kq3G8ubuI9rUgX5bqZbYZVuIan+5jTc9Wqx01X3T8xzawM
# 23WBjvtwxyTSJ1kQMYIB1TCCAdECAQEwNDAgMR4wHAYDVQQDDBV2YXNhbnRoQGxl
# YW5rbG91ZC5jb20CEENtxvnmq1WwRSZ3EQELvlAwCQYFKw4DAhoFAKB4MBgGCisG
# AQQBgjcCAQwxCjAIoAKAAKECgAAwGQYJKoZIhvcNAQkDMQwGCisGAQQBgjcCAQQw
# HAYKKwYBBAGCNwIBCzEOMAwGCisGAQQBgjcCARUwIwYJKoZIhvcNAQkEMRYEFGYI
# piY+D8gQfMaY5vn1xbo0peyCMA0GCSqGSIb3DQEBAQUABIIBAGNR9//LOLQGjq3K
# yVeyBtnZqOehGTy7QcLoxrs07apUeINeKOtENF+KddKYeN6LkugYLPBdnKo2kKFP
# ilVCMMPAmGw1jRbGrDHDpu5cPDgo9NRjcoboDWnLal6EWeUGccsP8XtXtNxXv/du
# HDdQIvfV6m9o8Ej5R3OzDEDVJFPRQqn7n4zfR1noygBERwlvihy2swxm0ZzjKqM/
# 9vqFVR2caBm7YdKcTq6RdTwHzMHrxh7aSoNjTkpN/1sxR6UAX8a8I3l9KC5Ljpql
# fNpGTlMXGNkdDqBBJKGzo8YjGEpfxONdkVicoLRJDYhNA84EWzPBllX3gR2mtdL9
# YaXoa6U=
# SIG # End signature block
