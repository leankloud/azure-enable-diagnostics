﻿# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


# MODIFICATIONS TO THIS FILE EFFECTED BY LEANKLOUD SOLUTIONS PRIVATE LIMITED DESCRIBED IN COMMENTS BELOW
# changes made to install the latest 3.0 version of linux diagnostic extension
# 3.0 version is available from publisher Microsoft.Azure.Diagnostics
# 3.0 expects the configuration as JSON instead of XML
# SAS token must be used instead of storage account key
# https://docs.microsoft.com/en-us/azure/virtual-machines/extensions/diagnostics-linux

New-Module -name CommonModule -ScriptBlock {

    function LoadSubscriptions() {
        $default = Login-AzureRmAccount
        return Get-AzureRmSubscription | Where {$_.State -eq "Enabled"}
    }

    function SelectSubscription() {
        Param (
            [Parameter(Mandatory=$true)]
            [string]$subscriptionId
        )

        Select-AzureRmSubscription -SubscriptionId $subscriptionId
	    Register-AzureRmResourceProvider -ProviderNamespace Microsoft.Insights
    }

    function LoadStorageAccounts {
        
        $allStorages = @{}

	    Write-Host("Getting existing storage accounts")
	    (Get-AzureRmStorageAccount | Where {$_.Sku.Tier -eq "Standard"} | Group-Object -Property Location) | foreach { $allStorages.Add($_.Name, $_.Group) }

        return $allStorages
    }

    function CreateStorageAccount {
    Param (
    		[Parameter(Mandatory=$true)]
    		[string] $ResourceGroupName,
    		[Parameter(Mandatory=$true)]
    		[string] $StorageName,
            [Parameter(Mandatory=$true)]
    		[string] $Location
    )

        return New-AzureRmStorageAccount -ResourceGroupName $ResourceGroupName -Name $StorageName -SkuName "Standard_LRS" -Location $Location
    }

    function LoadVirtualMachines() {
        Param (
            [string]$OsType
        )

        if ($OsType){
            return Get-AzureRmVM | where {$_.StorageProfile.OsDisk.OsType -eq $OsType}
        }
        
        return Get-AzureRmVM
    }

    function ReloadVm() {
        Param (
            [Parameter(Mandatory=$true)]
            [System.Object] $Vm
        )

        return Get-AzureRmVM -VMName $Vm.Name -ResourceGroupName $Vm.ResourceGroupName
    }

    function CreateVirtualMachineResultObject {
        Param (
            [Parameter(Mandatory=$true)]
		    [System.Object] $Vm
        )
    
        $virtualMachineProperties = @{
            'OsType' = $Vm.StorageProfile.OsDisk.OsType.ToString();
            'VmName' = $Vm.Name
            'ResourceGroupName' = $Vm.ResourceGroupName;
            'StorageAccountName' = "";
            'Result' = @{'Status' = $null; 'ReasonOfFailure' = $null};
            'IsOverriden' = $false
        }
    
        return New-Object –TypeName PSObject –Prop $virtualMachineProperties
    }

    function IsRunning() {
        Param (
    		[Parameter(Mandatory=$true)]
    		[System.Object] $Vm
    	)

        $status = Get-AzureRmVM -ResourceGroupName $Vm.ResourceGroupName -Name $Vm.Name -Status
        return [bool] ($status.Statuses | foreach { [bool] ($_.Code -eq "PowerState/running") } | where { $_ } | select -first 1)
    }

    function IsVmAgentReady() {
        Param (
    		[Parameter(Mandatory=$true)]
    		[System.Object] $Vm
    	)

        $status = (Get-AzureRmVM -ResourceGroupName $Vm.ResourceGroupName -Name $Vm.Name -Status).VMAgent
        return [bool] ($status.Statuses | foreach { [bool] ($_.DisplayStatus -eq "Ready") } | where { $_ } | select -first 1)
    }

    function IsDiagnosticsEnabled() {
       	Param (
    		[Parameter(Mandatory=$true)]
    		[System.Object] $Vm
    	)

        $diag = $null
        $ResourceGroupName = $vm.ResourceGroupName

        switch -regex ($Vm.StorageProfile.OsDisk.OsType) {
            "[Ww]indows" {$diag = Get-AzureRmVMDiagnosticsExtension -ResourceGroupName $ResourceGroupName -VMName $Vm.Name}
            "[Ll]inux" {

                $extensionName = $null
                $extension = $Vm.Extensions | Where {$_.VirtualMachineExtensionType -eq "LinuxDiagnostic"}

                if (!$extension) {
		            return $false
                }

                $diag = Get-AzureRmVMExtension -ResourceGroupName $ResourceGroupName -VMName $Vm.Name -Name $extension.Name -ErrorAction Ignore
            } 
            default {throw [System.InvalidOperationException] "$OsType is not supported. Allowed values are 'Windows' or 'Linux' "}
        }

        if ($diag -eq $null) {
		    return $false
	    }

	    $cfg = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String((ConvertFrom-Json -InputObject $diag.PublicSettings).xmlCfg))
	    if (([xml]$cfg).WadCfg.DiagnosticMonitorConfiguration.Metrics -eq $null) {
		    return $false
	    }

        return $true
    }

    function SetVmDiagnostic() {
        Param (
            [Parameter(Mandatory=$true)]
		    [System.Object] $Vm,
            [Parameter(Mandatory=$true)]
            [System.Object] $Storage,
            [Parameter(Mandatory=$true)]
            [string] $CfgPath
        )

        $ErrorActionPreference = "Stop"

        $vmName = $Vm.Name
        $ResourceGroupName = $Vm.ResourceGroupName
        $VmLocation =  $Vm.Location

        $storageName = $Storage.StorageAccountName
        # SAS token instead of storage key
        # https://docs.microsoft.com/en-us/azure/virtual-machines/extensions/diagnostics-linux#protected-settings
        $sasStart = Get-Date -date "2020-09-01"
        $sasEnd = Get-Date -date "2035-09-01"
        $storageSas = New-AzureStorageAccountSASToken -Service Blob,File,Table,Queue -ResourceType Service,Container,Object -Permission "racwdlup" -Context $Storage.Context -StartTime $sasStart -ExpiryTime $sasEnd

	    Write-Host("Enabling ARM diagnostics for '$VmName' virtual machine")

        Write-Host("Enabling diagnostics")
        switch -regex ($Vm.StorageProfile.OsDisk.OsType) {
            "[Ww]indows" {
                Set-AzureRmVMDiagnosticsExtension -ResourceGroupName $ResourceGroupName -VMName $vmName -DiagnosticsConfigurationPath $CfgPath -StorageAccountName $storageName -StorageAccountKey $storageKey -AutoUpgradeMinorVersion $true
            }
            "[Ll]inux" {

                $publicSetting = Get-Content $CfgPath | Out-String
                $privateSettingsString = '{"storageAccountName":"' + $storageName + '","storageAccountSasToken":"' + $storageSas + '"}';
                $extensionName = "LinuxDiagnostic"

                Write-Host("Using $extensionName as linux diagnostics extension name")
                Set-AzureRmVMExtension -ResourceGroupName $ResourceGroupName -VMName $vmName -Publisher "Microsoft.Azure.Diagnostics" -ExtensionType "LinuxDiagnostic" -Name $extensionName -Location $VmLocation -SettingString $publicSetting -ProtectedSettingString $privateSettingsString -TypeHandlerVersion "3.0"
            }
            default {throw [System.InvalidOperationException] "$OsType is not supported. Allowed values are 'Windows' or 'Linux' "}
        }

		Write-Host("Diagnostics enabled")
    }

    Export-ModuleMember -Function LoadSubscriptions
    Export-ModuleMember -Function SelectSubscription
    Export-ModuleMember -Function LoadStorageAccounts
    Export-ModuleMember -Function LoadVirtualMachines
    Export-ModuleMember -Function CreateVirtualMachineResultObject
    Export-ModuleMember -Function ReloadVm
    Export-ModuleMember -Function CreateStorageAccount
    Export-ModuleMember -Function IsRunning
    Export-ModuleMember -Function IsVmAgentReady
    Export-ModuleMember -Function IsDiagnosticsEnabled
    Export-ModuleMember -Function SetVmDiagnostic

} | Import-Module

# SIG # Begin signature block
# MIIFoQYJKoZIhvcNAQcCoIIFkjCCBY4CAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUVPQH6EWmA98ybydQ0XzHCHyc
# AKGgggM2MIIDMjCCAhqgAwIBAgIQQ23G+earVbBFJncRAQu+UDANBgkqhkiG9w0B
# AQsFADAgMR4wHAYDVQQDDBV2YXNhbnRoQGxlYW5rbG91ZC5jb20wHhcNMjAxMTEy
# MTc1MTE0WhcNMjExMTEyMTgxMTE0WjAgMR4wHAYDVQQDDBV2YXNhbnRoQGxlYW5r
# bG91ZC5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDZv9r1hfIR
# +w1mFzarNbNo/ECgRlSsNKYnBcO7VEQBaS7qNmLkmjyIpwiopm+IzWJRTt8vRYQA
# PTUzXhv+63rwEYPEgCANdrbkb258R8N5RpV7YcOI4mK5ePjJi5Jdr80824pkmi9b
# ZV6E1FYAFIMoy88CEpjWKKdTp4xXbL1yeiBARRc+xmOtfaqRxVNNCFb75MV4ymwi
# gKNoxxAZoN0br6aNUBkyF5pc5l/30RlvWLKzDXwoWf8FMx/dBfhY2jkLZHU4Sh9W
# xzYHYgq82rTlL60AomgiuTkNQvxMa/We5jVv78NzC/ZfAzBeXKIHSztHHlykavgz
# ziJKD+YObrPdAgMBAAGjaDBmMA4GA1UdDwEB/wQEAwIHgDATBgNVHSUEDDAKBggr
# BgEFBQcDAzAgBgNVHREEGTAXghV2YXNhbnRoQGxlYW5rbG91ZC5jb20wHQYDVR0O
# BBYEFOislcM6IsEcO5y4eUCfhLezZrWbMA0GCSqGSIb3DQEBCwUAA4IBAQDXi9+m
# wWZFS+VADMFRpNnsE9LeAf9DgsB5XxeYlwow5KKdloNR46RD+y9GpHlBIDIWuwiG
# HjW1M1ISuz01P3/+xm6FakHl1gqK6B0MM2a2iysyg8LtzFJr11qYyNSuYa5GriWO
# rW6sMdTGwGTMoZ0HtRulPBzpwgjGGJr8ZD9ChcEvbbv/4zgTSXtjgIMmSGq0Qh8R
# b/YOalPY8JEqTq2SRfqNxWl59ommGejaZYCxVXK5QzWmHBdlc+52LuvrVr65Ikvw
# M56uj98G9QE3Hgy1c/kq3G8ubuI9rUgX5bqZbYZVuIan+5jTc9Wqx01X3T8xzawM
# 23WBjvtwxyTSJ1kQMYIB1TCCAdECAQEwNDAgMR4wHAYDVQQDDBV2YXNhbnRoQGxl
# YW5rbG91ZC5jb20CEENtxvnmq1WwRSZ3EQELvlAwCQYFKw4DAhoFAKB4MBgGCisG
# AQQBgjcCAQwxCjAIoAKAAKECgAAwGQYJKoZIhvcNAQkDMQwGCisGAQQBgjcCAQQw
# HAYKKwYBBAGCNwIBCzEOMAwGCisGAQQBgjcCARUwIwYJKoZIhvcNAQkEMRYEFHY3
# 3RAzxuKdd8OUt794dcnDUeWSMA0GCSqGSIb3DQEBAQUABIIBAF7KYiPYIbBtMPv1
# H3h7OZ2k3dXGtzL1WGuxN8ZwJCOqWx6R9UFAVyNFJaWefRSlF54AJgH9EFP5LRdi
# dV9Br1e9g9SKXobSRZZWwQ44K2kksrk2eFzc1VNUJ60601o2WDcy8FFkQ7+kusL+
# AC9njrQwFJWjyQt4uAZ2bGYSBu9CX6PD8HtyZ6hRk0kZdTtLdSFiigYpmTTKjQKa
# 0khYpmJdCaO3kqdnXkQoyxhFoPqUW2CiuYQjsdE+5aPevEQWYzTMhUpJpCVjBhdv
# YQzSHda5LuI3WAPqp4CGo4KwztIgimlCU7RIer+Va97CH7Wi64h6l/hLEr7humwh
# gtJpeco=
# SIG # End signature block
